package com.example.demo.exception;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message, Serializable identifier, String type) {
        super(message);
        this.identifier = identifier;
        this.type = type;
    }

    private Serializable identifier;
    private String type;
}
