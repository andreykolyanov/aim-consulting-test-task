package com.example.demo.commons;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TimeService {

    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
