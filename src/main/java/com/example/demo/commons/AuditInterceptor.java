package com.example.demo.commons;

import com.example.demo.service.AuditLogService;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import javax.persistence.Id;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AuditInterceptor extends EmptyInterceptor {

    private final AuditLogService service;

    public AuditInterceptor(AuditLogService service) {
        this.service = service;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        service.add(createStateMap(previousState, propertyNames, id, types), createStateMap(currentState, propertyNames, id, types));
        return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
    }

    private Map<String, Object> createStateMap(Object[] properties, String[] propertyNames, Serializable id, Type[] types) {
        assert properties.length == propertyNames.length;
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        for (int i = 0; i < properties.length; i++) {
            if (!types[i].isAssociationType()) {
                map.put(propertyNames[i], properties[i]);
            } else if (types[i].isCollectionType()) {
                Set<Serializable> idSet = (Set<Serializable>) ((Collection)properties[i]).stream().map(this::getId).collect(Collectors.toSet());
                map.put(propertyNames[i], idSet);
            } else {
                map.put(propertyNames[i], getId(properties[i]));
            }
        }
        return map;
    }

    private Serializable getId(Object object) {
        try {
            Class clazz = object.getClass();
            Field idField = getIdField(clazz);
            assert idField != null;
            idField.setAccessible(true);
            return (Serializable) idField.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access exception", e);
        }
    }

    private Field getIdField(Class<?> clazz) {
        Field[] allFields = clazz.getDeclaredFields();

        for (Field field : allFields) {
            if(field.isAnnotationPresent(Id.class))
                return field;
        }
        return null;
    }
}
