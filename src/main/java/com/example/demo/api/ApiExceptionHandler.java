package com.example.demo.api;

import com.example.demo.api.dto.ErrorDto;
import com.example.demo.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorDto> handleException(EntityNotFoundException e) {
        return new ResponseEntity<>(new ErrorDto(e.getIdentifier(), e.getType()), HttpStatus.NOT_FOUND);
    }
}
