package com.example.demo.api;

import com.example.demo.api.converter.StudyGroupConverter;
import com.example.demo.api.dto.StudyGroupDto;
import com.example.demo.model.StudyGroup;
import com.example.demo.service.StudentService;
import com.example.demo.service.StudyGroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/group")
public class StudyGroupEndpoint {

    private final StudyGroupService service;
    private final StudyGroupConverter converter;
    private final StudentService studentService;

    public StudyGroupEndpoint(StudyGroupService service, StudyGroupConverter converter, StudentService studentService) {
        this.service = service;
        this.converter = converter;
        this.studentService = studentService;
    }

    @GetMapping
    public ResponseEntity<List<StudyGroupDto>> getAll() {
        List<StudyGroup> studyGroups = service.findAll();
        return ResponseEntity.ok(studyGroups.stream().map(converter::toDtoForListProjection).collect(Collectors.toList()));
    }

    @GetMapping(value = "/{guid}")
    public ResponseEntity<StudyGroupDto> get(@PathVariable("guid") String guid) {
        StudyGroup group = service.findByGuid(guid);
        return ResponseEntity.ok(converter.toDto(group));
    }

    @PutMapping
    public ResponseEntity<StudyGroupDto> put(@RequestBody StudyGroupDto dto) {
        StudyGroup group = service.create(dto.getGuid(), dto.getTitle());
        return ResponseEntity.ok(converter.toDto(group));
    }

    @PostMapping
    public ResponseEntity<StudyGroupDto> update(@RequestBody StudyGroupDto dto) {
        StudyGroup group = service.update(dto.getGuid(), dto.getTitle());
        return ResponseEntity.ok(converter.toDto(group));
    }

    @DeleteMapping(value = "/{guid}")
    public ResponseEntity delete(@PathVariable("guid") String guid) {
        StudyGroup group = service.findByGuid(guid);
        service.delete(group);
        return ResponseEntity.noContent().build();
    }
}
