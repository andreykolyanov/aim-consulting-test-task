package com.example.demo.api.converter;

import com.example.demo.api.dto.StudentDto;
import com.example.demo.model.Student;
import org.springframework.stereotype.Component;

@Component
public class StudentDtoConverter {

    public StudentDto toDto(Student student) {
        return StudentDto.builder()
                .dossierNumber(student.getDossierNumber())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .studyGroup(student.getStudyGroup().getGuid())
                .build();
    }
}
