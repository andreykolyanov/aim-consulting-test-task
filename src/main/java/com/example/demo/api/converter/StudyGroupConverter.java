package com.example.demo.api.converter;

import com.example.demo.api.dto.StudyGroupDto;
import com.example.demo.model.StudyGroup;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class StudyGroupConverter {

    private final StudentDtoConverter studentDtoConverter;

    public StudyGroupConverter(StudentDtoConverter studentDtoConverter) {
        this.studentDtoConverter = studentDtoConverter;
    }

    public StudyGroupDto toDto(StudyGroup studyGroup) {
        return StudyGroupDto.builder()
                .guid(studyGroup.getGuid())
                .title(studyGroup.getTitle())
                .students(studyGroup.getStudents().stream().map(studentDtoConverter::toDto).collect(Collectors.toList()))
                .build();
    }

    public StudyGroupDto toDtoForListProjection(StudyGroup studyGroup) {
        return StudyGroupDto.builder()
                .guid(studyGroup.getGuid())
                .title(studyGroup.getTitle())
                .build();
    }
}
