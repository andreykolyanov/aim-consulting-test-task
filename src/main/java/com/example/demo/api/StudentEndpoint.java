package com.example.demo.api;

import com.example.demo.api.converter.StudentDtoConverter;
import com.example.demo.api.dto.StudentDto;
import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import com.example.demo.service.StudentService;
import com.example.demo.service.StudyGroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/student")
public class StudentEndpoint {

    private final StudentService service;
    private final StudentDtoConverter converter;
    private final StudyGroupService studyGroupService;

    public StudentEndpoint(StudentService service, StudentDtoConverter converter, StudyGroupService studyGroupService) {
        this.service = service;
        this.converter = converter;
        this.studyGroupService = studyGroupService;
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>> getAll() {
        List<Student> students = service.findAll();
        return ResponseEntity.ok(students.stream().map(converter::toDto).collect(Collectors.toList()));
    }

    @GetMapping(value = "/{dossierNumber}")
    public ResponseEntity<StudentDto> get(@PathVariable("dossierNumber") Long dossierNumber) {
        Student student = service.findByDossierNumber(dossierNumber);
        return ResponseEntity.ok(converter.toDto(student));
    }

    @PutMapping
    public ResponseEntity<StudentDto> put(@RequestBody StudentDto dto) {
        StudyGroup studyGroup = studyGroupService.findByGuid(dto.getStudyGroup());
        Student student = service.create(dto.getDossierNumber(), dto.getFirstName(), dto.getLastName(), studyGroup);
        return ResponseEntity.ok(converter.toDto(student));
    }

    @PostMapping
    public ResponseEntity<StudentDto> update(@RequestBody StudentDto dto) {
        StudyGroup studyGroup = studyGroupService.findByGuid(dto.getStudyGroup());
        Student student = service.update(dto.getDossierNumber(), dto.getFirstName(), dto.getLastName(), studyGroup);
        return ResponseEntity.ok(converter.toDto(student));
    }

    @DeleteMapping(value = "/{dossierNumber}")
    public ResponseEntity delete(@PathVariable("dossierNumber") Long dossierNumber) {
        Student student = service.findByDossierNumber(dossierNumber);
        service.delete(student);
        return ResponseEntity.noContent().build();
    }
}
