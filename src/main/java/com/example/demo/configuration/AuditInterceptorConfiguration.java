package com.example.demo.configuration;

import com.example.demo.commons.AuditInterceptor;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuditInterceptorConfiguration implements HibernatePropertiesCustomizer {

    private final AuditInterceptor auditInterceptor;

    public AuditInterceptorConfiguration(AuditInterceptor auditInterceptor) {
        this.auditInterceptor = auditInterceptor;
    }

    @Override
    public void customize(Map<String, Object> hibernateProperties) {
        hibernateProperties.put("hibernate.session_factory.interceptor", auditInterceptor);
    }
}
