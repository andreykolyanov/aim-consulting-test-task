package com.example.demo.service;

import com.example.demo.dao.StudyGroupRepository;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class StudyGroupService {

    private final StudyGroupRepository repository;

    public StudyGroupService(StudyGroupRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public StudyGroup create(String guid, String title) {
        StudyGroup group = new StudyGroup(guid, title);
        return repository.save(group);
    }

    @Transactional
    public StudyGroup create(String guid, String title, Set<Student> students) {
        StudyGroup group = new StudyGroup(guid, title, students);
        return repository.save(group);
    }

    @Transactional
    public StudyGroup update(String guid, String title) {
        StudyGroup studyGroup = findByGuid(guid);
        studyGroup.setTitle(title);
        return repository.save(studyGroup);
    }

    @Transactional(readOnly = true)
    public List<StudyGroup> findAll() {
        return repository.findAll();
    }

    @Transactional
    public StudyGroup findByGuid(String guid) {
        return repository.findByGuid(guid).orElseThrow(() -> new EntityNotFoundException("Study group not found", guid, StudyGroup.class.getSimpleName()));
    }

    @Transactional
    public void delete(StudyGroup studyGroup) {
        repository.delete(studyGroup);
    }
}
