package com.example.demo.service;

import com.example.demo.dao.StudentRepository;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepository repository;

    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public Student create(Long dossierNumber, String firstName, String lastName, StudyGroup studyGroup) {
        Student student = new Student(dossierNumber, firstName, lastName, studyGroup);
        return repository.save(student);
    }

    @Transactional
    public Student update(Long dossierNumber, String firstName, String lastName, StudyGroup studyGroup) {
        Student student = findByDossierNumber(dossierNumber);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.attachToGroup(studyGroup);
        return repository.save(student);
    }

    @Transactional(readOnly = true)
    public List<Student> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Student findByDossierNumber(Long dossierNumber) {
        return repository.findByDossierNumber(dossierNumber).orElseThrow(() -> new EntityNotFoundException("Student not found", dossierNumber, Student.class.getSimpleName()));
    }

    public void delete(Student student) {
        repository.delete(student);
    }
}
