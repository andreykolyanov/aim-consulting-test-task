package com.example.demo.service;

import com.example.demo.commons.TimeService;
import com.example.demo.dao.AuditEntryRepository;
import com.example.demo.model.AuditLogEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AuditLogService {

    private final AuditEntryRepository repository;
    private final TimeService timeService;
    private final ObjectMapper objectMapper;

    public AuditLogService(AuditEntryRepository repository,
                           TimeService timeService,
                           ObjectMapper objectMapper) {
        this.repository = repository;
        this.timeService = timeService;
        this.objectMapper = objectMapper;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void add(Map<String, Object> previousState, Map<String, Object> currentState) {
        ObjectWriter writer = objectMapper.writerFor(HashMap.class);
        try {
            AuditLogEntry auditLogEntry = new AuditLogEntry(timeService.now(), writer.writeValueAsString(previousState), writer.writeValueAsString(currentState));
            repository.save(auditLogEntry);
        } catch (JsonProcessingException e) {
            log.error("Audit log not added. Can't write entity to json", e);
        }
    }


}
