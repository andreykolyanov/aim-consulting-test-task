package com.example.demo.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "student")
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class Student {

    public Student(Long dossierNumber, String firstName, String lastName, StudyGroup studyGroup) {
        this.dossierNumber = dossierNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.studyGroup = studyGroup;
    }

    @Id
    @SequenceGenerator(name = "student_seq_generator", sequenceName = "student_sequence", allocationSize = 1)
    @GeneratedValue(generator = "student_seq_generator")
    private Long id;

    @Getter
    @Setter
    @NaturalId
    @Column(name = "dossier_number")
    private Long dossierNumber;

    @Getter
    @Setter
    @Column(name = "first_name")
    private String firstName;

    @Getter
    @Setter
    @Column(name = "last_name")
    private String lastName;

    @Getter
    @Setter(value = AccessLevel.PACKAGE)
    @ManyToOne(cascade = CascadeType.MERGE)
    private StudyGroup studyGroup;

    public void attachToGroup(StudyGroup studyGroup) {
        this.studyGroup = studyGroup;
        studyGroup.addStudent(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id) &&
                dossierNumber.equals(student.dossierNumber) &&
                firstName.equals(student.firstName) &&
                lastName.equals(student.lastName) &&
                Objects.equals(studyGroup, student.studyGroup);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dossierNumber, firstName, lastName, studyGroup);
    }
}
