package com.example.demo.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "study_group")
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class StudyGroup {

    public StudyGroup(String guid, String title) {
        this.guid = guid;
        this.title = title;
        this.students = new HashSet<>();
    }

    public StudyGroup(String guid, String title, Set<Student> students) {
        this.guid = guid;
        this.title = title;
        this.students = students;
    }

    @Id
    @SequenceGenerator(name = "study_group_seq_generator", sequenceName = "study_group_sequence", allocationSize = 1)
    @GeneratedValue(generator = "study_group_seq_generator")
    private Long id;

    @Getter
    @Setter
    @Column(name = "guid")
    private String guid;

    @Getter
    @Setter
    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "studyGroup", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<Student> students;

    public Set<Student> getStudents() {
        return Collections.unmodifiableSet(students);
    }

    public void addStudent(Student student) {
        if (student.getStudyGroup() == null) {
            this.students.add(student);
            student.setStudyGroup(this);
        }
    }

    public void removeStudent(Student student) {
        if (student.getStudyGroup() != null) {
            this.students.remove(student);
            student.setStudyGroup(null);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudyGroup that = (StudyGroup) o;
        return Objects.equals(id, that.id) &&
                guid.equals(that.guid) &&
                title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, guid, title);
    }
}
