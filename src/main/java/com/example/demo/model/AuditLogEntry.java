package com.example.demo.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "audit_log")
public class AuditLogEntry {

    public AuditLogEntry(LocalDateTime timestamp, String before, String after) {
        this.timestamp = timestamp;
        this.before = before;
        this.after = after;
    }

    @Id
    @SequenceGenerator(name = "audit_seq_generator", sequenceName = "audit_sequence", allocationSize = 1)
    @GeneratedValue(generator = "audit_seq_generator")
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Type(type = "Jsonb")
    @Column(name = "before")
    private String before;

    @Type(type = "Jsonb")
    @Column(name = "after")
    private String after;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditLogEntry that = (AuditLogEntry) o;
        return Objects.equals(id, that.id) &&
                timestamp.equals(that.timestamp) &&
                before.equals(that.before) &&
                after.equals(that.after);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, before, after);
    }
}
