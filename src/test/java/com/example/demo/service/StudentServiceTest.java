package com.example.demo.service;

import com.example.demo.dao.StudentRepository;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class StudentServiceTest {

    private StudentRepository repository = mock(StudentRepository.class);

    @Test
    void testCreate() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        Student student = new Student(1L, "First", "Last", studyGroup);
        when(repository.save(student)).thenReturn(student);
        StudentService service = new StudentService(repository);
        Student actual = service.create(1L, "First", "Last", studyGroup);
        assertEquals(student, actual, "Save error.");
    }

    @Test
    void update() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        Student student = new Student(1L, "First", "Last", studyGroup);
        when(repository.save(student)).thenReturn(student);
        when(repository.findByDossierNumber(1L)).thenReturn(Optional.of(student));
        StudentService service = new StudentService(repository);
        Student actual = service.update(1L, "First", "Last", studyGroup);
        assertEquals(student, actual, "Update student error");
    }

    @Test
    void findAll() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        Student student = new Student(1L, "First", "Last", studyGroup);
        when(repository.findAll()).thenReturn(Collections.singletonList(student));
        StudentService service = new StudentService(repository);
        List<Student> students = service.findAll();
        assertEquals(1, students.size(), "Incorrect size of found students");
        assertEquals(student, students.get(0), "Incorrect student found");
    }

    @Test
    void findByDossierNumber() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        Student student = new Student(1L, "First", "Last", studyGroup);
        when(repository.findByDossierNumber(1L)).thenReturn(Optional.of(student));
        StudentService service = new StudentService(repository);
        Student actual = service.findByDossierNumber(1L);
        assertEquals(student, actual, "Incorrect student found");
    }

    @Test
    void findByDossierNumber_notExists() {
        when(repository.findByDossierNumber(1L)).thenReturn(Optional.empty());
        StudentService service = new StudentService(repository);
        assertThrows(EntityNotFoundException.class, () -> service.findByDossierNumber(1L));
    }

    @Test
    void delete() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        Student student = new Student(1L, "First", "Last", studyGroup);
        StudentService service = new StudentService(repository);
        service.delete(student);
        verify(repository).delete(student);
    }
}