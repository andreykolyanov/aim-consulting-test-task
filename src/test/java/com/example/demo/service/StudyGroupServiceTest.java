package com.example.demo.service;

import com.example.demo.dao.StudyGroupRepository;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class StudyGroupServiceTest {

    private StudyGroupRepository repository = mock(StudyGroupRepository.class);

    @Test
    void create() {
        StudyGroup studyGroup = new StudyGroup("guid", "title");
        when(repository.save(studyGroup)).thenReturn(studyGroup);
        StudyGroupService service = new StudyGroupService(repository);
        StudyGroup actual = service.create("guid", "title");
        assertEquals(studyGroup, actual, "Create error");
    }

    @Test
    void testCreate() {
        Set<Student> students = new HashSet<>();
        StudyGroup studyGroup = new StudyGroup("guid", "title", students);
        when(repository.save(studyGroup)).thenReturn(studyGroup);
        StudyGroupService service = new StudyGroupService(repository);
        StudyGroup actual = service.create("guid", "title", students);
        assertEquals(studyGroup, actual, "Create error");
    }

    @Test
    void update() {
        Student student = new Student(1L, "First", "Last", null);
        Set<Student> students = new HashSet<>();
        students.add(student);
        StudyGroup studyGroup = new StudyGroup("guid", "title", students);
        when(repository.save(studyGroup)).thenReturn(studyGroup);
        when(repository.findByGuid("guid")).thenReturn(Optional.of(studyGroup));
        StudyGroupService service = new StudyGroupService(repository);
        StudyGroup actual = service.update("guid", "title");
        assertEquals(studyGroup, actual, "Update error");
    }

    @Test
    void findAll() {
        StudyGroup studyGroup = new StudyGroup("guid", "title");
        when(repository.findAll()).thenReturn(Collections.singletonList(studyGroup));
        StudyGroupService service = new StudyGroupService(repository);
        List<StudyGroup> actual = service.findAll();
        assertEquals(1, actual.size(), "Incorrect size of found study group");
        assertEquals(studyGroup, actual.get(0), "Incorrect found study group");
    }

    @Test
    void findByGuid() {
        StudyGroup studyGroup = new StudyGroup("guid", "title");
        when(repository.findByGuid("guid")).thenReturn(Optional.of(studyGroup));
        StudyGroupService service = new StudyGroupService(repository);
        StudyGroup actual = service.findByGuid("guid");
        assertEquals(studyGroup, actual, "Incorrect find by guid");
    }

    @Test
    void findByGuid_notExists() {
        when(repository.findByGuid("guid")).thenReturn(Optional.empty());
        StudyGroupService service = new StudyGroupService(repository);
        assertThrows(EntityNotFoundException.class, () -> service.findByGuid("guid"));
    }

    @Test
    void delete() {
        StudyGroup studyGroup = new StudyGroup("guid", "title");
        StudyGroupService service = new StudyGroupService(repository);
        service.delete(studyGroup);
        verify(repository).delete(studyGroup);
    }
}