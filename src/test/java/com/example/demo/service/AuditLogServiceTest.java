package com.example.demo.service;

import com.example.demo.commons.TimeService;
import com.example.demo.dao.AuditEntryRepository;
import com.example.demo.model.AuditLogEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AuditLogServiceTest {

    private final AuditEntryRepository repository = mock(AuditEntryRepository.class);
    private final TimeService timeService = mock(TimeService.class);
    private final ObjectMapper objectMapper = mock(ObjectMapper.class);

    @Test
    void add() throws JsonProcessingException {
        LocalDateTime now = LocalDateTime.now();
        ObjectWriter writer = mock(ObjectWriter.class);
        when(timeService.now()).thenReturn(now);
        when(objectMapper.writerFor(HashMap.class)).thenReturn(writer);
        Map<String, Object> previous = new HashMap<>();
        previous.put("1", "1");
        Map<String, Object> current = new HashMap<>();
        current.put("1", "2");
        when(writer.writeValueAsString(previous)).thenReturn("prev");
        when(writer.writeValueAsString(current)).thenReturn("current");

        AuditLogService service = new AuditLogService(repository, timeService, objectMapper);
        service.add(previous, current);
        AuditLogEntry expected = new AuditLogEntry(now, "prev", "current");
        verify(repository).save(eq(expected));
    }

    @Test
    void add__processing_error() throws JsonProcessingException {
        LocalDateTime now = LocalDateTime.now();
        ObjectWriter writer = mock(ObjectWriter.class);
        when(timeService.now()).thenReturn(now);
        when(objectMapper.writerFor(HashMap.class)).thenReturn(writer);
        Map<String, Object> previous = new HashMap<>();
        previous.put("1", "1");
        Map<String, Object> current = new HashMap<>();
        current.put("1", "2");
        when(writer.writeValueAsString(previous)).thenThrow(JsonProcessingException.class);

        AuditLogService service = new AuditLogService(repository, timeService, objectMapper);
        service.add(previous, current);
        AuditLogEntry expected = new AuditLogEntry(now, "prev", "current");
        verify(repository, times(0)).save(eq(expected));
    }
}