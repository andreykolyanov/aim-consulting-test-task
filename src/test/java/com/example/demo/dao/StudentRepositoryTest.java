package com.example.demo.dao;

import com.example.demo.model.Student;
import com.example.demo.model.StudyGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private StudentRepository studentRepository;

    @Test
    void findByDossierNumber() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        entityManager.persist(studyGroup);
        entityManager.flush();

        Student student = new Student(1L, "First", "Last", studyGroup);
        entityManager.persist(student);
        entityManager.flush();

        Optional<Student> studentOptional = studentRepository.findByDossierNumber(1L);

        assertTrue(studentOptional.isPresent());
    }

    @Test
    void update__with_not_valid_json() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        entityManager.persist(studyGroup);
        entityManager.flush();

        Student student = new Student(1L, "First", "Last", studyGroup);
        entityManager.persist(student);
        entityManager.flush();

        Optional<Student> studentOptional = studentRepository.findByDossierNumber(1L);
        Student savedStudent = studentOptional.orElseThrow(EntityNotFoundException::new);
        savedStudent.setFirstName("First{");
        studentRepository.save(savedStudent);

        Optional<Student> updatedStudentOptional = studentRepository.findByDossierNumber(1L);

        assertTrue(updatedStudentOptional.isPresent());
        assertEquals(savedStudent, updatedStudentOptional.get(), "Update incorrect json object error");
    }
}