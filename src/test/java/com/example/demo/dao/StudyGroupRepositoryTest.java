package com.example.demo.dao;

import com.example.demo.model.StudyGroup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class StudyGroupRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private StudyGroupRepository studyGroupRepository;

    @Test
    void findByGuid() {
        StudyGroup studyGroup = new StudyGroup("7b", "7Б");
        entityManager.persist(studyGroup);
        entityManager.flush();

        Optional<StudyGroup> studyGroupOptional = studyGroupRepository.findByGuid("7b");
        assertTrue(studyGroupOptional.isPresent());
    }
}