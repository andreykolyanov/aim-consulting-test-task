package com.example.demo.configuration;

import org.hibernate.dialect.PostgreSQL95Dialect;

import java.sql.Types;

public class TestExtendedPostgreSQLDialect extends PostgreSQL95Dialect {

    public TestExtendedPostgreSQLDialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "varchar");
    }
}
